function change(){

    let opc1 = document.getElementById('monedaOrigen').value;
    let opc2 = document.getElementById('monedaDestino');
    let opciones = ""

    if(opc1 == 'mxn'){
        opciones = '<option value="da">Dólar Americano</option><option value="dc">Dólar Canadiense</option><option value="eur">Euro</option>';
    }else if(opc1 == 'da'){
        opciones = '<option value="mxn">Peso Mexicano</option><option value="dc">Dólar Canadiense</option><option value="eur">Euro</option>';
    }else if(opc1 == 'dc'){
        opciones = '<option value="mxn">Peso Mexicano</option><option value="da">Dólar Americano</option><option value="eur">Euro</option>';
    }else if(opc1 == 'eur'){
        opciones = '<option value="mxn">Peso Mexicano</option><option value="da">Dólar Americano</option><option value="dc">Dólar Canadiense</option>';
    }else if(opc1 == 'v'){
        opciones = '<option value="v">-- Moneda Destino --</option>';
    }

    opc2.innerHTML = opciones;
}

function calcular(){

    let cantidad = document.getElementById('cantidad').value;
    let monedaOrigen = document.getElementById('monedaOrigen').value;
    let monedaDestino = document.getElementById('monedaDestino').value;

    var total = 0;

    // Dólar americano a los demás
    if(monedaOrigen == 'da' && monedaDestino == 'mxn'){
        total = cantidad * 19.85;
    }else if(monedaOrigen == 'da' && monedaDestino == 'dc'){
        total = cantidad * 1.35;
    }else if(monedaOrigen == 'da' && monedaDestino == 'eur'){
       total = cantidad * 0.99;
    }

    // Dólar canadiense a los demás
    if(monedaOrigen == 'dc' && monedaDestino == 'mxn'){
        total = cantidad / 1.35;
        total = total*19.85;
    }else if(monedaOrigen == 'dc' && monedaDestino == 'da'){
        total = cantidad / 1.35;
    }else if(monedaOrigen == 'dc' && monedaDestino == 'eur'){
        total = cantidad / 1.35;
        total = total * 0.99;
    }

    // Pesos mexicanos a los demás
    if(monedaOrigen == 'mxn' && monedaDestino == 'da'){
        total = cantidad / 19.85;
    }else if(monedaOrigen == 'mxn' && monedaDestino == 'dc'){
        total = cantidad / 19.85;
        total = total * 1.35;
    }else if(monedaOrigen == 'mxn' && monedaDestino == 'eur'){
        total = cantidad / 19.85;
        total = total * 0.99;
    }

    // Euro a los demás
    if(monedaOrigen == 'eur' && monedaDestino == 'mxn'){
        total = cantidad / .99;
        total = total*19.85;
    }else if(monedaOrigen == 'eur' && monedaDestino == 'da'){
        total = cantidad / 0.99;
    }else if(monedaOrigen == 'eur' && monedaDestino == 'dc'){
        total = cantidad / 0.99;
        total = total * 1.35;
    }

    let subtotal = total;
    let totalCom = subtotal * 0.03;
    let totalPagar = subtotal + totalCom;

    let subTexto = document.getElementById('subtotal');
    let tComTexto = document.getElementById('totalComision');
    let tPagTexto = document.getElementById('totalPagar');

    // Mostrar la información
    subTexto.innerHTML = subTexto.setAttribute("value", subtotal);
    tComTexto.innerHTML = tComTexto.setAttribute("value", totalCom,);
    tPagTexto.innerHTML = tPagTexto.setAttribute("value", totalPagar);

}

// Variables globales que se van a usar para guardar el total de los registros
var subtotalF = 0;
var totalComisionF = 0;
var totalPagarF = 0;

function registro(){

    let cantidad = document.getElementById('cantidad').value;
    let monedaOrigen = document.getElementById('monedaOrigen').value;
    let monedaDestino = document.getElementById('monedaDestino').value;

    var total = 0;

    // Dólar americano a los demás
    if(monedaOrigen == 'da' && monedaDestino == 'mxn'){
        total = cantidad * 19.85;
    }else if(monedaOrigen == 'da' && monedaDestino == 'dc'){
        total = cantidad * 1.35;
    }else if(monedaOrigen == 'da' && monedaDestino == 'eur'){
       total = cantidad * 0.99;
    }

    // Dólar canadiense a los demás
    if(monedaOrigen == 'dc' && monedaDestino == 'mxn'){
        total = cantidad / 1.35;
        total = total*19.85;
    }else if(monedaOrigen == 'dc' && monedaDestino == 'da'){
        total = cantidad / 1.35;
    }else if(monedaOrigen == 'dc' && monedaDestino == 'eur'){
        total = cantidad / 1.35;
        total = total * 0.99;
    }

    // Pesos mexicanos a los demás
    if(monedaOrigen == 'mxn' && monedaDestino == 'da'){
        total = cantidad / 19.85;
    }else if(monedaOrigen == 'mxn' && monedaDestino == 'dc'){
        total = cantidad / 19.85;
        total = total * 1.35;
    }else if(monedaOrigen == 'mxn' && monedaDestino == 'eur'){
        total = cantidad / 19.85;
        total = total * 0.99;
    }

    // Euro a los demás
    if(monedaOrigen == 'eur' && monedaDestino == 'mxn'){
        total = cantidad / .99;
        total = total*19.85;
    }else if(monedaOrigen == 'eur' && monedaDestino == 'da'){
        total = cantidad / 0.99;
    }else if(monedaOrigen == 'eur' && monedaDestino == 'dc'){
        total = cantidad / 0.99;
        total = total * 1.35;
    }
    
    // Con esto conseguir los label donde se van a ingresar los datos
    let lblCantidad = document.getElementById('lblCantidad');
    let lblMonedaOrigen = document.getElementById('lblMonedaOrigen');
    let lblMonedaDestino = document.getElementById('lblMonedaDestino');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    let subtotal = total;
    let totalCom = subtotal * 0.03;
    let totalPagar = subtotal + totalCom;

    // Para conseguir que moneda se usó de origen
    var monedaOrigenT;
    if (monedaOrigen == 'mxn') {
        monedaOrigenT = "Peso mexicano a";
    }else if (monedaOrigen == 'da') {
        monedaOrigenT = "Dólar americano a";
    }else if (monedaOrigen == 'dc') {
        monedaOrigenT = "Dólar canadiense a";
    }else if (monedaOrigen == 'eur') {
        monedaOrigenT = "Euro a";
    }

    // Para conseguir que moneda se usó de destino
    var monedaDestinoT;
    if (monedaDestino == 'mxn') {
        monedaDestinoT = "Peso mexicano";
    }else if (monedaDestino == 'da') {
        monedaDestinoT = "Dólar americano";
    }else if (monedaDestino == 'dc') {
        monedaDestinoT = "Dólar canadiense";
    }else if (monedaDestino == 'eur') {
        monedaDestinoT = "Euro";
    }

    // Mostrar la información en los labels seleccionados
    lblCantidad.innerHTML = lblCantidad.innerHTML + cantidad + "<br>";
    lblMonedaOrigen.innerHTML = lblMonedaOrigen.innerHTML + monedaOrigenT + "<br>";
    lblMonedaDestino.innerHTML = lblMonedaDestino.innerHTML + monedaDestinoT + "<br>";
    lblSubtotal.innerHTML = lblSubtotal.innerHTML + subtotal + "<br>";
    lblTotalComision.innerHTML = lblTotalComision.innerHTML + totalCom + "<br>";
    lblTotalPagar.innerHTML = lblTotalPagar.innerHTML + totalPagar + "<br>";

    // Conseguir la suma de los datos
    let regSubtotal = document.getElementById('registroSubtotal');
    let regTotalComision = document.getElementById('registroTotalComision');
    let regTotalPagar = document.getElementById('registroTotalPagar');

    subtotalF = subtotalF + subtotal;

    regSubtotal.innerText = ""
    regSubtotal.innerText = regSubtotal.innerText + subtotalF;

    totalComisionF = totalComisionF + totalCom;

    regTotalComision.innerText = ""
    regTotalComision.innerText = regTotalComision.innerText + totalComisionF;

    totalPagarF = totalPagarF + totalPagar;

    regTotalPagar.innerText = ""
    regTotalPagar.innerText = regTotalPagar.innerText + totalPagarF;

}

function borrarRegistros(){
    let lblCantidad = document.getElementById('lblCantidad');
    let lblMonedaOrigen = document.getElementById('lblMonedaOrigen');
    let lblMonedaDestino = document.getElementById('lblMonedaDestino');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    lblCantidad.innerHTML =""
    lblMonedaOrigen.innerHTML =""
    lblMonedaDestino.innerHTML =""
    lblSubtotal.innerHTML =""
    lblTotalComision.innerHTML =""
    lblTotalPagar.innerHTML =""

    let regSubtotal = document.getElementById('registroSubtotal');
    let regTotalComision = document.getElementById('registroTotalComision');
    let regTotalPagar = document.getElementById('registroTotalPagar');

    regSubtotal.innerText = "---"
    subtotalF = 0;

    regTotalComision.innerText = "---"
    totalComisionF = 0;

    regTotalPagar.innerText = "---"
    totalPagarF = 0;

}